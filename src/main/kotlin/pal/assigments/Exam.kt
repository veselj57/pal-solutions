package pal

import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader
import java.util.*
import kotlin.collections.ArrayList
import kotlin.system.measureTimeMillis

fun main() {

    val alg = Exam(System.`in`)


    println(alg.compute())
}

data class Vertex(
    val index: Int = 0,
    val weight: Int = 0,
    val connections: MutableSet<Edge> = mutableSetOf(),
    val reversed: MutableSet<Edge> = mutableSetOf(),
    var visited:Boolean = false,
    var order: Int = Int.MAX_VALUE
)

data class Edge(
    var from: Int,
    var to: Int
) {
    fun swap() {
        val tmp = from
        from = to
        to = tmp
    }
}

data class Solution(
    val edge: Edge,
    val weight: Int,
    var vertexes: List<Vertex> = listOf()
)


class Exam(val stream: InputStream) {

    var solution: Solution? = null

    val nodesCount: Int
    val edgesCount: Int

    val vertexes: Array<Vertex?>


    init {
        val input = BufferedReader(InputStreamReader(stream), 8192 * 16)
        val settings = input.readLine().split(" ")

        nodesCount = settings[0].toInt()
        edgesCount = settings[1].toInt()

        vertexes = Array(nodesCount){ null}
        repeat(nodesCount) {
            vertexes[it] = Vertex(
                index = it,
                weight = input.readLine()!!.trim().toInt()
            )

        }

        repeat(edgesCount) {
            val (start, end) = input.readLine()!!.split(" ").map { it.trim().toInt() }

            vertexes[start]!!.connections.add(Edge(start, end))
            vertexes[end]!!.reversed.add(Edge(end, start))
        }
    }

    fun compute(): String {
        val a = topologicalSort(false)

        var solution: Solution? = null

        for (v in vertexes){
            for (e in v?.connections!!){

                val set = a.subList(a.indexOf(vertexes[e.from]), a.indexOf(vertexes[e.to])+1).toSet()

                val sum = dfs(e, vertexes[e.from]!!, set)

                //println(sum)

                if ((solution != null && solution.weight < sum ) || solution == null)
                    solution = Solution(e, sum)
            }
        }

//        println(a.joinToString { it.index.toString() })


        solution!!.vertexes.forEach {
            println("${it.index} W: ${it.weight}")
        }


        return "${solution.edge.from} ${solution.edge.to} ${solution.weight}"
    }

    fun dfs(e: Edge, vertex: Vertex, set: Set<Vertex>): Int{
        if (!set.contains(vertex))
            return 0

        var sum = vertex.weight
        vertex.connections.forEach {
            if (it != e)
                sum +=  dfs(e, vertexes[it.to]!!, set)
        }

        return sum
    }

    fun topologicalSort(v: Vertex?, stack: Stack<Vertex>, reversed: Boolean) {
        if (v == null || v.visited)
            return

        v.visited = true

        val edges = if (reversed) v.reversed else v.connections

        for (edge in edges){
            val next = vertexes[edge.to] ?: continue
            topologicalSort(next, stack, reversed)
        }

        stack.push(v)
    }

    fun topologicalSort(reversed: Boolean): List<Vertex> {
        val stack = Stack<Vertex>()

        for (v in vertexes)
            topologicalSort(v, stack, reversed)

    val order = TreeSet<Pair<Int, Vertex>> { a, b -> a.first - b.first }

        order.addAll(stack.toList().reversed().mapIndexed {index, vertex -> Pair(index, vertex) })


        return stack.toList().reversed()
    }


}
